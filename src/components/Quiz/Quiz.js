import React, { useState } from 'react';
import styles from './Quiz.module.css';
import Question from '../Question/Question';

export default function Quiz() {
    const [questions, updateQuestions] = useState([
        { question: { text: 'stuff1', id: 0 }, choices: ['1', '2', '3'] },
        { question: { text: 'stuff2', id: 1 }, choices: ['1', '2', '3'] }
    ]);

    const [answers, updateAnswers] = useState({});

    function setQuestionValue(id, answer) {
        updateAnswers({
            ...answers,
            [id]: answer
        });
    }

    function submit() {
        // TODO: post answers
    }

    return (
        <div>
            {questions.map(({ question, choices }, i) => (
                <Question
                    key={i}
                    question={question}
                    choices={choices}
                    setQuestionValue={setQuestionValue}
                />
            ))}
            <button onClick={submit}>Submit</button>
        </div>
    );
}
