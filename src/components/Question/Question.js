import React from 'react';
import styles from './Question.module.css';

export default function Question({ question, choices, setQuestionValue }) {
    return (
        <div>
            <h2>{question.text}</h2>
            <ul>
                {choices.map((choice, i) => (
                    <li key={i}>
                        <label>
                            <input
                                name={question.id}
                                type="radio"
                                value={choice}
                                onChange={(e) =>
                                    setQuestionValue(
                                        question.id,
                                        e.target.value
                                    )
                                }
                            />
                            {choice}
                        </label>
                    </li>
                ))}
            </ul>
        </div>
    );
}
