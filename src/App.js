import React, { Component } from 'react';
import TopNavBar from './components/TopNavBar/TopNavBar';
import Quiz from './components/Quiz/Quiz';

class App extends Component {
    render() {
        return (
            <div className="App">
                <TopNavBar />
                <Quiz />
            </div>
        );
    }
}

export default App;
